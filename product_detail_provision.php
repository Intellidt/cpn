<html>
    <head>
    </head>
    <body>
        <?php
        include('header.php');
        ?>
        <!--image height same as home slider-->
        <div class="container-fluid m-50">
            <img src="">
        </div>
        <div class="container">
                <p></p>
            <div class="row m-10">
            <h4 style="font-weight:00 ; " class="text-left">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</h4>

                <div class="col-lg-4 col-md-12">
                    <img src="images/temp2.png">
                    <h3>Lutein</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>
                <div class="col-lg-4 col-md-12">
                    <img src="images/temp2.png">
                    <h3>Vitamin A</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>
                <div class="col-lg-4 col-md-12">
                    <img src="images/temp2.png">
                    <h3>Vitamin B1</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>
            </div>
            <div class="row m-50">
                <div class="col-lg-4 col-md-12">
                    <img src="images/temp2.png">
                    <h3>Vitamin B2</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>
                <div class="col-lg-4 col-md-12">
                    <img src="images/temp2.png">
                    <h3>Ascorbic acid</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>
                <div class="col-lg-4 col-md-12">
                    <img src="images/temp2.png">
                    <h3>Vitamin D3</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>
            </div>
        </div>
        <div class="container-fluid bg-aqua m-50" >
            <div class="container">
                <div class="row py-4">
                    <div class="col-lg-3">
                        <img src="images/temp1.png">
                        <h2>Sugar,Gluten & Dairy Free</h2>
                    </div>
                    <div class="col-lg-3">
                        <img src="images/temp1.png">
                        <h2>Non GMO</h2>
                    </div>
                    <div class="col-lg-3">
                        <img src="images/temp1.png">
                        <h2>No Artificial Additives</h2>
                    </div>
                    <div class="col-lg-3">
                        <img src="images/temp1.png">
                        <h2>No Preservatives</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
        <h2 class="text-left">Supplement Facts</h2>
            <div class="row">
                <div class="col-lg-6 col-md-12">
                        <p class="bg-gray">Servicing Size: 1softgel</p>
                    <!-- <ul>
                        <li class="bg-gray">Servicing Size: 1softgel</li>
                    </ul> -->
                </div>
                <div class="col-lg-3 col-sm-12">
                    <p class="bg-gray">AMOUNT PER SERVING</p>
                    <!-- <ul>
                        <li class="bg-gray">AMOUNT PER SERVING</li>
                    </ul> -->
                </div>
                <div class="col-lg-3 col-sm-12">
                <p class="bg-gray">% DAILY VALUE</p>
                    <!-- <ul>
                        <li class="bg-gray">% DAILY VALUE</li>
                    </ul> -->
                </div>
            </div>
        </div>
        <?php
        include('footer.php');
        ?>
    </body>
</html>