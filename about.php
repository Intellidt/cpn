
<?php
    @include("inc/header.php");
?>

<script>
function redirect_contact()
{
     location.href = "/cpn/contact.php";
} 
</script>


    <div class="container-fluid mb-5">
        <img src="images/about/banner_about.jpg" class="img-fluid ">
            <div class="carousel-caption smallcarouselcaption">
             <h2>Why Canadian Pinnacle Nutritech? </h2>
            </div>
    </div>
    <div class="container">
        <p class="text-justify mb-5">At Canadian Premier Standard, we believe in delivering top quality vitamins, manufactured to the highest safety standards. Not all vitamins are created equal; our firm works with the leading licensed manufacturer in Canada. We make our vitamins with the purest ingredients respecting the highest safety-standards. We spare no effort to make sure you get the highest quality product. All of our supplements come from Western Canada’s leading pharmaceutically-inspected facility. In business for over 20 years, our manufacturer, as a member of a highly-regulated industry within Canada, regularly passes audits by Health Canada, the US FDA and has also been certified Halal. We work hard to bring you the safest and healthiest supplements possible.</p>
    </div>
    
    
<!-- bottom info starts -->
        
        <div class="container-fluid bg-aliceblue py-5 mb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <img src="images/icon_quality.png" class="mx-auto d-block">
                    <h5 class="text-center">Quality Control</h5>
                    <h4 class="color-black text-center">Our Vancouver based manufacturing facility is pharmaceutically-inspected</h4>
                </div>
                <div class="col-lg-4 col-md-12 ">
                    <img src="images/icon_fda.png" class="mx-auto d-block">
                    <h5 class="text-center">Raw Materials Compliance </h5>
                    <h4 class="color-black text-center">AAll of our raw materials have the Certificate Of Authenticity and approved by the US FDA</h4>
                </div>
                <div class="col-lg-4 col-md-12">
                    <img src="images/icon_canadian_made.png" class="mx-auto d-block">
                    <h5 class="text-center">Canadian Made</h5>
                    <h4 class="color-black text-center">All of our products are 100% made and package in Vancouver, BC, Canada</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container mb-5">
        <h2 class="color-orange text-center">Interested in becoming Canadian Pinnacle Nutritech products distributor? </h2>
        <div class="row">
            <div class="col text-center mt-4 mb-2">
            <button type="button" class="btn btn-outline-secondary button-overlay rounded-0" onclick="redirect_contact()">CONTACT US</button>
                <!--<button type="button" class="btn btn-outline-secondary button-overlay rounded-0" onclick="redirect_contact()">CONTACT US</button>-->
            </div>
        </div>
    </div>
    
<!-- bottom info ends -->

    <?php
    include('inc/footer.php');
    ?>