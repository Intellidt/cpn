
        <?php
        include('header.php');
        ?>
        <!--image height same as home slider-->
        <div class="container-fluid m-50">
            <img src="images/Product Detail Provision/productdetail_banner.jpg" class="img-fluid ">
            <div class="carousel-caption">
                
            </div>
        </div>
        <!--below banner-->
        <div class="container">
                
            
        </div>
        <div class="container-fluid bg-aliceblue m-50" >
            <div class="container">
                <div class="row py-4">
                    <div class="col-lg-3">
                        <img src="images/Product Detail Provision/product_detail_icon_dairy_free.png" class="d-block mx-auto">
                        <h2 class="text-center">Sugar,Gluten & Dairy Free</h2>
                    </div>
                    <div class="col-lg-3">
                        <img src="images/Product Detail Provision/product_detail_icon_non_gmo.png" class="d-block mx-auto">
                        <h2 class="text-center">Non GMO</h2>
                    </div>
                    <div class="col-lg-3">
                        <img src="images/Product Detail Provision/product_detail_icon_no_artificial.png" class="d-block mx-auto">
                        <h2 class="text-center">No Artificial Additives</h2>
                    </div>
                    <div class="col-lg-3">
                        <img src="images/Product Detail Provision/product_detail_icon_no_preservative.png" class="d-block mx-auto">
                        <h2 class="text-center">No Preservatives</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <h1 class="text-left m-50">Supplement Facts</h1>
            <table class="table">
                <thead>
                    <tr>
                    <th scope="col" style="width:50%; margin-left:20px;" class="bg-linkwater">Servicing Size: 1 softgel</th>
                    <th scope="col" style="width:25%" class="text-uppercase text-center bg-linkwater">amount per serving</th>
                    <th scope="col" style="width:25%"class="text-uppercase text-center bg-linkwater">%daily value</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <td>Maca Root P.E 0.6% Glucosinolates</td>
                    <td class="text-center">150.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>L-Arginine</td>
                    <td class="text-center">50.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Deer Horn</td>
                    <td class="text-center">30.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Ginkgo Biloba 24% Flavonol glycol/6% Terpene</td>
                    <td class="text-center">60.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Zinc</td>
                    <td class="text-center">10.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Garlic Ext.</td>
                    <td class="text-center">10.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Pumpkin Seed Oil</td>
                    <td class="text-center">200.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Vitamin E (d-alpha tocopherol)</td>
                    <td class="text-center">25.00 IU</td>
                    <td class="text-center"></td>
                    </tr>
                </tbody>
            </table>
            <h2 class="text-left">Recommended Dose</h2>
            <p class="text-left">Take 1 Softgels orally with a glass of water 30-60 minutes before activity, or as recommended by your healthcare practitioner</p>
            <p class="text-left" style="font-family:Montserrat Italic;font-size:14pt;">Note: Store in a cool, dry place.  Avoid direct sunlight or elevated humidity</p>
            <p class="text-left" style="font-family:Montserrat Italic;font-size:14pt;">Caution & Warnings:Consult a health care practitioner if you are taking prescription medication.</p>

        </div>

        <?php
        include('footer.php');
        ?>
    </body>
</html>