<?php
        include('inc/header.php');
        ?>
  
<!-- banner starts --->        
<div class="container-fluid mb-5"> <img src="images/product/banner_lovrbido.jpg" class="img-fluid ">
  <div class="carousel-caption productcarouselcaption">
    <h2 class="color-red text-left">Lovrbido</h2>
    <p>TBC</p>
    <p><strong>120 Softgels</strong></p>
  </div>
</div>

<!-- banner ends ---> 


<!-- vitamin starts --->

<!-- vitamin ends --->

<!-- 4 factors starts --->

<?php
        include('inc/product_factors.php');
        ?>

<!-- 4 factors ends --->

<div class="container">
  
  <h6 class="mb-5">Supplement Facts</h6>
            <table class="table">
                      <tr class="text-uppercase text-center bg-linkwater">
                        <th scope="col" style="width:50%">Servicing Size: 1 softgel</th>
                        <th scope="col" style="width:25%">Amount per Serving</th>
                        <th scope="col" style="width:25%">% Daily Value</th>
                      </tr>
                    <tr>
                    <td>Maca Root P.E 0.6% Glucosinolates</td>
                    <td class="text-center">150.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>L-Arginine</td>
                    <td class="text-center">50.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Deer Horn</td>
                    <td class="text-center">30.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Ginkgo Biloba 24% Flavonol glycol/6% Terpene</td>
                    <td class="text-center">60.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Zinc</td>
                    <td class="text-center">10.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Garlic Ext.</td>
                    <td class="text-center">10.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Pumpkin Seed Oil</td>
                    <td class="text-center">200.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Vitamin E (d-alpha tocopherol)</td>
                    <td class="text-center">25.00 IU</td>
                    <td class="text-center"></td>
                    </tr>
            </table><BR />
  <h6>Recommended Dose</h6>
            <p>Take 1 Softgels orally with a glass of water 30-60 minutes before activity, or as recommended by your healthcare practitioner</p>
            <p><em>Note: Store in a cool, dry place.  Avoid direct sunlight or elevated humidity<BR />
            Caution & Warnings:Consult a health care practitioner if you are taking prescription medication.</em></p>

        </div>

        <?php
        include('inc/footer.php');
        ?>