<html>
<head>
<meta content="width=device-width, initial-scale=1" name="viewport" />
<script src ="js/jquery.min.js"></script>
<script src ="js/bootstrap.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid" >
  <!--menu-->
  <div class="container" style="position: relative;">
    <nav class="navbar navbar-expand-lg  m-10">
      <img src="images/logo_cpn.png" alt="" class="logo">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarcontent" aria-controls="navbarcontent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon "></span> </button>
      <div class="collapse navbar-collapse" id="navbarcontent">
        <div class="navbar-nav-wrapper" style="width:30%">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active" style="width:50%"><a class="nav-link" id="home" href="/cpn/">Home</a> </li>
            <li class="nav-item" style="width:50%"><a class="nav-link" id="aboutus" href="/cpn/about.php">About Us</a> </li>
          </ul>
        </div>
        <div class="navbar-brand d-none d-lg-block" style="width:40% ;"><img src="images/logo_cpn.png" alt="" style="display:block; margin:auto;"></div>
        <div class="navbar-nav-wrapper" style="width:30%">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown subproducts" style="width:50%"><a class="nav-link" href="#products" data-toggle="collapse" >Products</a>
            </li>
            <li class="nav-item dropdown subproducts1" style="width:50%">
                                <a class="nav-link text-uppercase color-black" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Products
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="/cpn/product_optphysique.php" style="font-family:Playfair Display Regular;font-size:15pt;">OptPhysique</a>
                                <a class="dropdown-item" href="/cpn/product_immunotonia.php" style="font-family:Playfair Display Regular;font-size:15pt;">Immunotonia</a>
                                <a class="dropdown-item" href="/cpn/product_liveria.php" style="font-family:Playfair Display Regular;font-size:15pt;">Liveria</a>
                                <a class="dropdown-item" href="/cpn/product_provision.php" style="font-family:Playfair Display Regular;font-size:15pt;">ProVision</a>
                                <a class="dropdown-item" href="/cpn/product_lovebido.php" style="font-family:Playfair Display Regular;font-size:15pt;">Lovebido</a>
                                </div>
              </li>
            <li class="nav-item " style="width:50%"><a class="nav-link" id="contact" href="/cpn/contact.php">Contact</a></li>
          </ul>
        </div>
      </div>
    </nav>
      <div id="products" class="collapse">
          <div class="d-inline-block product-box">
              <img src="images/Home/home_bottle_lovebido.jpg" class="d-block m-auto">
              <a class="" href="/cpn/product_optphysique.php">OptPhysique</a>
          </div>
          <div class="d-inline-block product-box">
              <img src="images/Home/home_bottle_lovebido.jpg" class="d-block m-auto">
              <a class="" href="/cpn/product_immunotonia.php">Immunotonia</a>
          </div>
          <div class="d-inline-block product-box">
              <img src="images/Home/home_bottle_lovebido.jpg" class="d-block m-auto">
              <a class="" href="/cpn/product_liveria.php">Liveria</a>
          </div>
          <div class="d-inline-block product-box">
              <img src="images/Home/home_bottle_lovebido.jpg" class="d-block m-auto">
              <a class="" href="/cpn/product_provision.php">ProVision</a>
          </div>
          <div class="d-inline-block product-box">
              <img src="images/Home/home_bottle_lovebido.jpg" class="d-block m-auto">
              <a class="" href="/cpn/product_lovebido.php">Lovebido</a>
          </div>
      </div>
  </div>
</div>
