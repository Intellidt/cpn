<?php
        include('inc/header.php');
        ?>
  
<!-- banner starts --->        
<div class="container-fluid mb-5"> <img src="images/product/banner_optphysique.jpg" class="img-fluid ">
  <div class="carousel-caption productcarouselcaption">
    <h2 class="color-red text-left">Optphysique</h2>
    <p>TBC</p>
    <p><strong>120 Softgels</strong></p>
  </div>
</div>

<!-- banner ends ---> 


<!-- vitamin starts --->
        <div class="container">
               
            <div class="row mb-5">
            
                <div class="col-lg-4 col-md-12">
                    <img src="images/product/icon_vitamin_b1.png" class="mb-2"><BR />
                    <h2>Vitamin B1</h2>
                    <p>Enables the body to use carbohydrates as energy. It is essential for glucose metabolism, and it plays a key role in nerve, muscle, and heart function.</p>
                </div>
                <div class="col-lg-4 col-md-12">
                    <img src="images/product/icon_vitamin_c.png" class="mb-2"><BR />
                    <h2>Ascorbic acid</h2>
                    <p>Also known as Vitamin C, plays an important role in maintaining a healthy immune system, tissue and cell repair and collagen production. Also helps your body absorb iron.</p>
                </div>
                <div class="col-lg-4 col-md-12">
                   <p>&nbsp;</p> 
                </div>
            </div>   
        </div>
<!-- vitamin ends --->

<!-- 4 factors starts --->

<?php
        include('inc/product_factors.php');
        ?>

<!-- 4 factors ends --->

<div class="container">
  
  <h6 class="mb-5">Supplement Facts</h6>
            <table class="table">
                      <tr class="text-uppercase text-center bg-linkwater">
                        <th scope="col" style="width:50%">Servicing Size: 1 softgel</th>
                        <th scope="col" style="width:25%">Amount per Serving</th>
                        <th scope="col" style="width:25%">% Daily Value</th>
                      </tr>
                    <tr>
                    <td>CLA</td>
                    <td class="text-center">400.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>HCA (Garcinia  Cambogia PE 60%)</td>
                    <td class="text-center">225.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Vitamin C</td>
                    <td class="text-center">12.50 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Vitamin B1</td>
                    <td class="text-center">0.275 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Vitamin B6</td>
                    <td class="text-center">0.375 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Vitamin B5</td>
                    <td class="text-center">1.25 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Vitamin B3</td>
                    <td class="text-center">3.75 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Zinc</td>
                    <td class="text-center">2.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                </tbody>
            </table><BR />
  <h6>Recommended Dose</h6>
            <p>Take 1 Softgels orally with a glass of water in the morning, or as recommended by your healthcare practitioner</p>
            <p><em>Note: Store in a cool, dry place.  Avoid direct sunlight or elevated humidity<BR />
            Caution & Warnings: Consult a health care practitioner if you are taking prescription medication.</em></p>

        </div>

        <?php
        include('inc/footer.php');
        ?>