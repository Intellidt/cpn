
<?php
    @include("inc/header.php");
?>

<!-- <div id="pagedetail"> -->

        <!--slider starts-->
        <div id="carouselExampleIndicators" class="carousel slide mb-5" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
            </ol>
            <div class="carousel-inner">
            
            <!---banner starts-->
                <div class="carousel-item active" >
                    <img src="images/home/banner_home_optphysique.jpg" class="img-fluid " alt="First slide">
                    <div class="homecarouselcaption">
                        <div class="homecarousel-box">
                            <h1 class="color-orange">OptPhysique</h1>
                            <img src="images/home/curve.png" width="208" height="29" />
                        </div>
                        <h3 class="color-orange homecarousel-subtitle">Nature's way to manage your weights and increase your energy levels</h3>
                        <BR />
                        <p><button class=" btn btn-warning color-white rounded-0" onclick="redirect_optphysique()">VIEW DETAILS</button></p>
                    </div>
                </div>
                <div class="carousel-item">
                <img src="images/home/banner_home_immunotonia.jpg" class="img-fluid " alt="First slide">
                    <div class="homecarouselcaption">
                        <div class="homecarousel-box">
                            <h1 class="color-orange">Immunotonia</h1>
                            <img src="images/home/curve.png" width="208" height="29" />
                        </div>
                        <h3 class="color-orange homecarousel-subtitle">Highly effective antioxidants for skin maintenance</h3>
                        <BR />
                        <p><button class=" btn btn-warning color-white rounded-0" onclick="redirect_immunotonia()">VIEW DETAILS</button></p>
                    </div>
                </div>
                <div class="carousel-item">
                <img src="images/home/banner_home_liveria.jpg" class="img-fluid " alt="First slide">
                    <div class="homecarouselcaption">
                        <div class="homecarousel-box">
                            <h1 class="color-orange">Liveria</h1>
                            <img src="images/home/curve.png" width="208" height="29" />
                        </div>
                        <h3 class="color-orange homecarousel-subtitle">Selected formula to help you maintain and support your liver</h3>
                        <BR />
                        <p><button class=" btn btn-warning color-white rounded-0" onclick="redirect_liveria()">VIEW DETAILS</button></p>
                    </div>
                </div>
                <div class="carousel-item">
                <img src="images/home/banner_home_provision.jpg" class="img-fluid " alt="First slide">
                    <div class="homecarouselcaption">
                        <div class="homecarousel-box">
                            <h1 class="color-orange">ProVision</h1>
                            <img src="images/home/curve.png" width="208" height="29" />
                        </div>
                        <h3 class="color-orange homecarousel-subtitle">Helps to promote good eye health and maintain eyesight</h3>
                        <BR />
                        <p><button class=" btn btn-warning color-white rounded-0" onclick="redirect_provision()">VIEW DETAILS</button></p>
                    </div>
                </div>
                <div class="carousel-item">
                <img src="images/home/banner_home_lovrbido.jpg" class="img-fluid " alt="First slide">
                    <div class="homecarouselcaption">
                        <div class="homecarousel-box">
                            <h1 class="color-orange">Lovrbido</h1>
                            <img src="images/home/curve.png" width="208" height="29" />
                        </div>
                        <h3 class="color-orange homecarousel-subtitle">Great way to increase blood circulation and optimize performance</h3>
                        <BR />
                        <p><button class=" btn btn-warning color-white rounded-0" onclick="redirect_lovrbido()">VIEW DETAILS</button></p>
                    </div>
                </div>
                
            <!---banner ends-->    
            
            </div>
        </div>
<!--slider starts-->


        <div class="container">
           <h2 class="text-center mb-5">Only the best quality ingredient goes into our products </h2>
           <p align="center" class ="mb-5">At Canadian Pinnacle Nutritech, we spare no effort to make sure you get the highest quality product. All of our supplements come from Western Canada’s leading pharmaceutically-inspected facility. In business for over 20 years, our manufacturer, as a member of a highly-regulated industry within Canada, regularly passes audits by Health Canada, the US FDA and has also been certified Halal.</p>

<!---products starts-->  
            <div class="row mt-5 mb-5">
                <div class="col-lg-4 product-box">
                    <img src="images/home/home_optphysique.png" class="mx-auto d-block m-20">
                    <div class="overlay">
                        <div class="m-3"style="border:1px solid white; height:92%;">
                            <h3 class="overlayheading">OptPhysique</h3>
                            <h4 class="overlaytext text-center color-white">Nature’s way to manage your weights and increase your energy levels. </h4>
                            <div class="col text-center mt-4 overlaybutton">
                                <button type="button" class="btn btn-outline-light color-white button-overlay rounded-0" onclick="redirect_optphysique()">VIEW DETAILS</button>
                            </div>
                        </div>
                    </div>
                    <h3 class="color-darkgray text-center">OptPhysique</h3>
                </div>
                <div class="col-lg-4 product-box">
                    <img src="images/home/home_immunotonia.png" class="mx-auto d-block m-20">
                    <div class="overlay">
                        <div class="m-3"style="border:1px solid white; height:92%;">
                            <h3 class="overlayheading">Immunotonia</h3>
                            <h4 class="overlaytext text-center color-white">Highly effective antioxidants for skin maintenance.</h4>
                            <div class="col text-center mt-4 overlaybutton">
                                <button type="button" class="btn btn-outline-light color-white button-overlay rounded-0" onclick="redirect_immunotonia()">VIEW DETAILS</button>
                            </div>
                    </div>
                    </div>
                    <h3 class="color-darkgray text-center">Immunotonia</h3>
                </div>
                <div class="col-lg-4 product-box">
                    <img src="images/home/home_immunotonia.png" class="mx-auto d-block m-20">
                    <div class="overlay">
                        <div class="m-3"style="border:1px solid white; height:92%;">
                            <h3 class="overlayheading">Liveria</h3>
                            <h4 class="overlaytext text-center color-white">Selected formula to help you maintain and support your liver.</h4>
                            <div class="col text-center mt-4 overlaybutton">
                                <button type="button" class="btn btn-outline-light color-white button-overlay rounded-0" onclick="redirect_liveria()">VIEW DETAILS</button>
                            </div>
                        </div>
                    </div>
                    <h3 class="color-darkgray text-center">Liveria</h3>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-lg-4 product-box">
                    <img src="images/home/home_provision.png" class="mx-auto d-block m-20">
                    <div class="overlay">
                        <div class="m-3"style="border:1px solid white; height:92%;">
                            <h3 class="overlayheading">Provision</h3>
                            <h4 class="overlaytext text-center color-white">Helps to promote good eye health and maintain eyesight</h4>
                            <div class="col text-center mt-4 overlaybutton">
                                <button type="button" class="btn btn-outline-light color-white button-overlay rounded-0" onclick="redirect_provision()">VIEW DETAILS</button>
                            </div>
                    </div>
                    </div>
                    <h3 class="color-darkgray text-center">ProVision</h3>  
                </div>
                <div class="col-lg-4 product-box">
                    <img src="images/home/home_lovrbido.png" class="mx-auto d-block m-20">
                    <div class="overlay">
                        <div class="m-3"style="border:1px solid white; height:92%;">
                            <h3 class="overlayheading">Lovrbido</h3>
                            <h4 class="overlaytext text-center color-white">Great way to increase blood circulation and optimize performance</h4>
                            <div class="col text-center mt-4 overlaybutton">
                                <button type="button" class="btn btn-outline-light color-white button-overlay rounded-0" onclick="redirect_lovrbido()">VIEW DETAILS</button>
                            </div>
                        </div>
                    </div>
                    <h3 class="color-darkgray text-center">Lovrbido</h3>
                </div>
                
<!---products ends-->

                
                <div class="col-lg-4">
                </div>
            </div>
        </div>
        
        
<!-- bottom info starts -->
        
        <div class="container-fluid bg-aliceblue py-5 mb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <img src="images/icon_quality.png" class="mx-auto d-block">
                    <h5 class="text-center">Quality Control</h5>
                    <h4 class="color-black text-center">Our Vancouver based manufacturing facility is pharmaceutically-inspected</h4>
                </div>
                <div class="col-lg-4 col-md-12 ">
                    <img src="images/icon_fda.png" class="mx-auto d-block">
                    <h5 class="text-center">Raw Materials Compliance </h5>
                    <h4 class="color-black text-center">AAll of our raw materials have the Certificate Of Authenticity and approved by the US FDA</h4>
                </div>
                <div class="col-lg-4 col-md-12">
                    <img src="images/icon_canadian_made.png" class="mx-auto d-block">
                    <h5 class="text-center">Canadian Made</h5>
                    <h4 class="color-black text-center">All of our products are 100% made and package in Vancouver, BC, Canada</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container mb-5">
        <h2 class="color-orange text-center">Interested in becoming Canadian Pinnacle Nutritech products distributor? </h2>
        <div class="row">
            <div class="col text-center mt-4 mb-2">
            <button type="button" class="btn btn-outline-info button-overlay rounded-0" onclick="redirect_contact()">CONTACT US</button>
                <!--<button type="button" class="btn btn-outline-secondary button-overlay rounded-0" onclick="redirect_contact()">CONTACT US</button>-->
            </div>
        </div>
    </div>
    
<!-- bottom info ends -->
    
    
    <?php
    include('inc/footer.php');
    ?>

