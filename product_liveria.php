<?php
        include('inc/header.php');
        ?>
  
<!-- banner starts --->        
<div class="container-fluid mb-5"> <img src="images/product/banner_liveria.jpg" class="img-fluid ">
  <div class="carousel-caption productcarouselcaption">
    <h2 class="color-red text-left">Liveria</h2>
    <p>TBC</p>
    <p><strong>120 Softgels</strong></p>
  </div>
</div>

<!-- banner ends ---> 


<!-- vitamin starts --->

<div class="container">
  <div class="row mb-5">
    <div class="col-lg-4 col-md-12"><img src="images/product/icon_vitamin_b1.png" class="mb-2"><BR />
                    <h2>Vitamin B1</h2>
                    <p>Enables the body to use carbohydrates as energy. It is essential for glucose metabolism, and it plays a key role in nerve, muscle, and heart function.</p>
    </div>
    <div class="col-lg-4 col-md-12"><img src="images/product/icon_vitamin_b2.png" class="mb-2"><BR />
                    <h2>Vitamin B2</h2>
                    <p>Vitamin B2 helps break down proteins, fats, and carbohydrates. Benefits include maintaining healthy blood cells, boosting energy levels, facilitating in a healthy metabolism and more.</p>
                </div>
                <div class="col-lg-4 col-md-12">
                    <p>&nbsp;</p>
                </div>
                
            </div>  
        </div>
        
<!-- vitamin ends --->

<!-- 4 factors starts --->

<?php
        include('inc/product_factors.php');
        ?>

<!-- 4 factors ends --->

<div class="container">
  
  <h6 class="mb-5">Supplement Facts</h6>
            <table class="table">
                      <tr class="text-uppercase text-center bg-linkwater">
                        <th scope="col" style="width:50%">Servicing Size: 1 softgel</th>
                        <th scope="col" style="width:25%">Amount per Serving</th>
                        <th scope="col" style="width:25%">% Daily Value</th>
                      </tr>
                    <tr>
                    <td>Silymarin</td>
                    <td class="text-center">150.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Vitamin B1</td>
                    <td class="text-center">4.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Vitamin B2</td>
                    <td class="text-center">4.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Vitamin B6</td>
                    <td class="text-center">4.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Vitamin B12</td>
                    <td class="text-center">2.00 mcg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Vitamin B3</td>
                    <td class="text-center">12.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Vitamin B5</td>
                    <td class="text-center">8.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Iron </td>
                    <td class="text-center">10.09 mg</td>
                    <td class="text-center"></td>
                    </tr>
            </table><BR />
  <h6>Recommended Dose</h6>
            <p>Take 1-2 softgels orally with a glass of water daily, or as recommended by your healthcare practitioner</p>
            <p ><em>Note: Store in a cool, dry place.  Avoid direct sunlight or elevated humidity <BR />
            Caution & Warnings: Consult a health care practitioner if you are taking prescription medication. Consult a health care practitioner if you are pregnant, breast feeding.</em></p>

        </div>

        <?php
        include('inc/footer.php');
        ?>