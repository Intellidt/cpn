<?php
        include('inc/header.php');
        ?>
  
<!-- banner starts --->        
<div class="container-fluid mb-5"> <img src="images/product/banner_immunotonia.jpg" class="img-fluid ">
  <div class="carousel-caption productcarouselcaption">
    <h2 class="color-red text-left">Immunotonia</h2>
    <p>TBC</p>
    <p><strong>120 Softgels</strong></p>
  </div>
</div>

<!-- banner ends ---> 


<!-- vitamin starts --->
<!-- vitamin ends --->

<!-- 4 factors starts --->

<?php
        include('inc/product_factors.php');
        ?>

<!-- 4 factors ends --->



        <div class="container">
           <h6 class="mb-5">Supplement Facts</h6>
                  <table class="table">
                      <tr class="text-uppercase text-center bg-linkwater">
                        <th scope="col" style="width:50%">Servicing Size: 1 softgel</th>
                        <th scope="col" style="width:25%">Amount per Serving</th>
                        <th scope="col" style="width:25%">% Daily Value</th>
                      </tr>
                    <tr>
                    <td>Resveratrols</td>
                    <td class="text-center">100.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Grape Seed Extract</td>
                    <td class="text-center">100.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Citrus Bioflavonoid </td>
                    <td class="text-center">50.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
                    <tr>
                    <td>Green Tea P.E.  98/80/50/1  </td>
                    <td class="text-center">100.00 mg</td>
                    <td class="text-center"></td>
                    </tr>
            </table><BR />
  <h6>Recommended Dose</h6>
            <p>Take 1 softgels orally with a glass of water daily, or as recommended by your healthcare practitioner</p>
            <p><em>Note: Store in a cool, dry place.  Avoid direct sunlight or elevated humidity<BR />
            Caution & Warnings: Consult a health care practitioner if you are taking prescription medication.</em></p>

        </div>

        <?php
        include('inc/footer.php');
        ?>