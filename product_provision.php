<?php
        include('inc/header.php');
        ?>
  
<!-- banner starts --->        
<div class="container-fluid mb-5"> <img src="images/product/banner_provision.jpg" class="img-fluid ">
  <div class="carousel-caption productcarouselcaption">
    <h2 class="color-red text-left">ProVision</h2>
    <p>All-in-one eye vitamin and multivitamin supplement. ProVision contains 20mg of Lutein that acts as an antioxidant and supports eye health, plus multivitamin that helps replenish vital nutrients to cover the gaps in your daily diet.</p>
    <p><strong>120 Softgels</strong></p>
  </div>
</div>

<!-- banner ends ---> 


<!-- vitamin starts --->

<div class="container">
  <div class="row mb-5">
    <h3 class="mb-5">Formulated with other vitamins to enhance the benefits of lutein. ProVision will help to reduce the risk of developing cataracts, age-related macular degeneration, and to improve macular pigment optimal density.</h3>
    <div class="col-lg-4 col-md-12"> <img src="images/product/icon_vitamin_lutein.png" class="mb-2"><BR />
      <h2>Lutein</h2>
      <p>Modern life is full of screen time and blue light exposure. Lutein is a powerful antioxidant made from natural marigold extract that acts as an antioxidant and supports eye health.</p>
    </div>
    <div class="col-lg-4 col-md-12"> <img src="images/product/icon_vitamin_a.png" class="mb-2"><BR />
      <h2>Vitamin A</h2>
      <p>Vitamin A is important for normal vision, the immune system, and reproduction. Vitamin A also helps the heart, lungs, kidneys, and other organs work properly.</p>
    </div>
    <div class="col-lg-4 col-md-12"> <img src="images/product/icon_vitamin_b1.png" class="mb-2"><BR />
      <h2>Vitamin B1</h2>
      <p>Enables the body to use carbohydrates as energy. It is essential for glucose metabolism, and it plays a key role in nerve, muscle, and heart function.</p>
    </div>
  </div>
  <div class="row mb-5">
    <div class="col-lg-4 col-md-12"> <img src="images/product/icon_vitamin_b2.png" class="mb-2"><BR />
      <h2>Vitamin B2</h2>
      <p>Vitamin B2 helps break down proteins, fats, and carbohydrates. Benefits include maintaining healthy blood cells, boosting energy levels, facilitating in a healthy metabolism and more.</p>
    </div>
    <div class="col-lg-4 col-md-12"> <img src="images/product/icon_vitamin_d3.png" class="mb-2"><BR />
      <h2>Ascorbic acid</h2>
      <p>Also known as Vitamin C, plays an important role in maintaining a healthy immune system, tissue and cell repair and collagen production. Also helps your body absorb iron.</p>
    </div>
    <div class="col-lg-4 col-md-12"><p>&nbsp;</p>
    </div>
  </div>
</div>

<!-- vitamin ends --->

<!-- 4 factors starts --->

<?php
        include('inc/product_factors.php');
        ?>

<!-- 4 factors ends --->

<div class="container">
  
  <h6 class="mb-5">Supplement Facts</h6>
  <table class="table">
      <tr class="text-uppercase text-center bg-linkwater">
        <th scope="col" style="width:50%">Servicing Size: 1 softgel</th>
        <th scope="col" style="width:25%">Amount per Serving</th>
        <th scope="col" style="width:25%">% Daily Value</th>
      </tr>
      <tr>
        <td>Lutein (from Tagetes erecta or marigold flower oleoresin)</td>
        <td class="text-center">20 mg</td>
        <td class="text-center"></td>
      </tr>
      <tr>
        <td>Vitamin A (Vitamin A palmitate)</td>
        <td class="text-center">698.08 mcg</td>
        <td class="text-center"></td>
      </tr>
      <tr>
        <td>Vitamin B1 (Thiamine)</td>
        <td class="text-center">1.5 mg</td>
        <td class="text-center"></td>
      </tr>
      <tr>
        <td>Vitamin B2 (Riboflavin)</td>
        <td class="text-center">1.5 mg</td>
        <td class="text-center"></td>
      </tr>
      <tr>
        <td>Ascorbic acid (Vitamin C, Ascorbic Acid, Calcium ascorbate)</td>
        <td class="text-center">100 mg</td>
        <td class="text-center"></td>
      </tr>
      <tr>
        <td>Vitamin D3 (Vitamin D, Cholecalciferol)</td>
        <td class="text-center">5 mcg</td>
        <td class="text-center"></td>
      </tr>
      <tr>
        <td colspan="3"><HR />
          <em>Non-Medical Ingredient(s): Lecithin Oil, Yellow bees wax<BR />
          Softgel Capsules: Gelatin, Glycerin, Water, Annatto Powder</em></td>
      </tr>
  </table><BR />
  <h6>Recommended Dose</h6>
  <p>Take 1 softgels orally with a glass of water daily, or as recommended by your healthcare practitioner</p>
  <P><em>Note: Store in a cool, dry place.  Avoid direct sunlight or elevated humidity<BR />
    Caution & Warnings: Consult a health care practitioner if you are taking prescription medication.</em></p>
</div>


<?php
        include('inc/footer.php');
        ?>