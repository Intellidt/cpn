<div class="container-fluid bg-aliceblue mb-5" >
  <div class="container">
    <div class="row py-4">
      <div class="col-lg-3"> <img src="images/product/icon_dairy_free.png" class="d-block mx-auto">
        <h5 class="text-center">Sugar, Gluten &amp; Dairy Free</h5>
      </div>
      <div class="col-lg-3"> <img src="images/product/icon_non_gmo.png" class="d-block mx-auto">
        <h5 class="text-center">Non GMO</h5>
      </div>
      <div class="col-lg-3"> <img src="images/product/icon_no_artificial.png" class="d-block mx-auto">
        <h5 class="text-center">No Artificial Additives</h5>
      </div>
      <div class="col-lg-3"> <img src="images/product/icon_no_preservative.png" class="d-block mx-auto">
        <h5 class="text-center">No Preservatives</h5>
      </div>
    </div>
  </div>
</div>